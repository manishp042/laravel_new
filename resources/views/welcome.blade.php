@extends('layouts.main_layout')
@section('content')  
 <div class="container">
 <!---heading---->
     @if(Session::has('success'))
     <div class="alert alert-success">
      <strong>Success!</strong> {{ Session::get('success') }}.
    </div>     
    @endif
     <header class="heading"> Registration For Your favourite Game</header><hr></hr>
    <!---Form starting----> 
    <form method="post" action="{{url('/')}}/submit_form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row ">
         <!--- For Name---->
         <div class="col-sm-12">
             <div class="row">
                 <div class="col-xs-4">
                     <label class="firstname">Full Name</label> </div>
                 <div class="col-xs-8">
                     <input type="text" name="name"  placeholder="Enter your Fll Name" class="form-control " required>
                </div>
              </div>
         </div>
         
         
         <div class="col-sm-12">
             <div class="row">
                 <div class="col-xs-4">
                     <label class="lastname">Email</label></div>
                <div class ="col-xs-8">  
                     <input type="email" name="email" id="email" placeholder="Enter your Email" class="form-control" required=>
                </div>
             </div>
         </div>
     <!-----For email---->
         <div class="col-sm-12">
             <div class="row">
                 <div class="col-xs-4">
                     <label class="mail" >Select Game:</label></div>
                 <div class="col-xs-8"  >    
                      <select name="game_id" class="form-control" id="game">
                        @foreach($games as $game)
                          <option value="{{$game->id}}">{{$game->name}}</option>
                        @endforeach
                      </select>
                 </div>
             </div>
         </div>
               
     <!-----------For Phone number-------->
         <div class="col-sm-12">         
                 <input type="submit" value="Submit" class="btn btn-warning btn-sm btn-block" id="submit" />
         </div>
     </div>
      </form>  
                 
         
</div>
@endsection

     
     