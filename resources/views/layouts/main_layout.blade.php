<!Doctype html>
<html>
<head>
     <meta charset="UTF-8">
     <title>Registration Form</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{ asset ('css/bootstrap.min.css') }}" rel="stylesheet" id="bootstrap-css">
        <link href="{{ asset ('css/custom.css') }}" rel="stylesheet" >
		<script src="{{ asset ('js/jquery.min.js') }}"></script>
		<script src="{{ asset ('js/bootstrap.min.js') }}"></script>
</head>
<body>
	@yield('content')  
</body>
</html>