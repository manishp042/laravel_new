<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use App\Registration;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Game $games)
    {   
        $games = $games->all();
        return view('welcome',compact("games"));
    }

    public function submit_form(Request $request){
       Registration::create($request->all());
       return Redirect('/')->with('success', "We have recieved your request, We will contact you soon");
    }
}
