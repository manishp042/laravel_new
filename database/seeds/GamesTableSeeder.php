<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('games')->truncate();
         $games = array("Cricket","Tennis","Volleyball","Badminton");				  
         foreach($games as $game){
        	DB::table('games')->insert(['name'=>$game]);
	    }    		
    }
}
